/*
 * Copyright (C) 2001,2002 Red Hat, Inc.
 *
 * This is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Library General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this program; if not, write to the Free Software
 * Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

*/
/*  zvti18n.c - Zed's Virtual Terminal
 *  Copyright (C) 1998  Michael Zucchi
 *
 *  i18n functions for the zvtterm widget.
 *
  Author: Naoyuki Ishimura <naoyuki.ishimura@sun.com>
	  Hidetoshi Tajima <hidetoshi.tajima@sun.com>

   Copyright (C) 2002 Sun Microsystems, Inc.

   The Gnome Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Library General Public License as
   published by the Free Software Foundation; either version 2 of the
   License, or (at your option) any later version.

   The Gnome Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Library General Public License for more details.

   You should have received a copy of the GNU Library General Public
   License along with the Gnome Library; see the file COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 59 Temple Place - Suite 330,
   Boston, MA 02111-1307, USA.
*/
#include <stdlib.h>
#include <string.h>
#include <wchar.h>
#include <gtk/gtkmain.h>
#include <gdk/gdkx.h>
#include <libzvt/libzvt.h>
#include <gtk/gtkimmulticontext.h>
#include <pango/pangox.h>

#ifdef HAS_I18N

static gboolean
localechar_iswide_internal (gchar *mbchar, int mb_len)
{
  wchar_t wchar_buf[1];
  gint wc_width;
  gboolean ret = FALSE;

  if (mbchar && mbtowc (wchar_buf, mbchar, mb_len) > 0)
    {
      wc_width = wcwidth (wchar_buf[0]);
      ret = wc_width > 1 ? TRUE : FALSE;
    }

  return ret;
}

gboolean
zvt_localechar_iswide (gunichar c)
{
#ifdef USE_PANGO_LAYOUT
  return g_unichar_iswide (c);
#else
  gchar utf8_buf[6];
  wchar_t wchar_buf[1];
  gint wc_width;
  gsize bytes_written;
  GError *io_error = NULL;
  gint len;
  gchar *mbchar;
  gboolean ret = FALSE;

  if (c < 0x80)			/* ascii */
    return ret;

  len = g_unichar_to_utf8 (c, utf8_buf);
  utf8_buf[len] = 0;
  mbchar = zvt_term_locale_from_utf8 (utf8_buf, len,
				      NULL, &bytes_written, &io_error);

  ret = localechar_iswide_internal (mbchar, bytes_written);

  g_free (mbchar);

  return ret;
#endif /* USE_PANGO_LAYOUT */
}

gint
zvt_column_preedit_string (gchar *utf8_ptr, gint length) {
  gunichar uc;
  gchar *p = utf8_ptr;
  gint count = 0;

  do {
    if (*p == 0)
      break;
    
    uc = g_utf8_get_char(p);
    if (g_unichar_iswide (uc))
      count++;
    count++;
  } while ((p = g_utf8_next_char(p)) && (p - utf8_ptr) < length);

  return count;
}

gint
zvt_column_from_utf8 (gchar *utf8_ptr, gint length) {
  gunichar uc;
  gchar *p = utf8_ptr;
  gint count = 0;

  do {
    if (*p == 0)
      break;
    
    uc = g_utf8_get_char(p);
    if (zvt_localechar_iswide (uc))
      count++;
    count++;
  } while ((p = g_utf8_next_char(p)) && (p - utf8_ptr) < length);

  return count;
}

void
zvt_term_get_baseline_offset (ZvtTerm *term, int row, int col,
			      gint *offset_x, gint *offset_y)
{
  /* rendering offsetx */
  *offset_x = col * term->charwidth;

#ifdef USE_PANGO_LAYOUT
  /* pango origin does not need to be added ascent */
  *offset_y = row * term->charheight;
#else
  *offset_y = row * term->charheight + term->font_ascent;
#endif
  return;
}

#include <locale.h>

static gboolean
zvt_term_is_c_locale ()
{
  gchar *locale = setlocale (LC_CTYPE, NULL);
  return (strcmp ("C", locale) == 0 || strcmp ("POSIX", locale) == 0);
}

gchar *
zvt_term_locale_from_utf8 (const gchar *utf8_ptr, gssize utf8_len,
			   gsize *bytes_read, gsize *bytes_written,
			   GError **error)
{
  gchar *locale_format;
  if (zvt_term_is_c_locale())
    locale_format = g_convert (utf8_ptr, utf8_len,
			       "ISO-8859-1", "UTF-8",
			       bytes_read, bytes_written, error);
  else
    locale_format = g_locale_from_utf8 (utf8_ptr, utf8_len, bytes_read,
					bytes_written, error);
  return locale_format;
}

gchar *
zvt_term_locale_to_utf8 (const gchar *locale_ptr, gssize locale_len,
			 gsize *bytes_read, gsize *bytes_written,
			 GError **error)
{
  gchar *utf8_format;
  if (zvt_term_is_c_locale())
    utf8_format = g_convert (locale_ptr, locale_len,
			     "UTF-8", "ISO-8859-1",
			     bytes_read, bytes_written, error);
  else
    utf8_format = g_locale_to_utf8 (locale_ptr, locale_len, bytes_read,
				    bytes_written, error);
  return utf8_format;
}

void
zvt_term_draw_text (ZvtTerm *term, struct vt_line *line, int row, int col,
		    gchar *utf8_ptr, int len, int x, int y, int offx,
		    int offy, GdkGC *fgc, int dofill, int overstrike)
{
  GtkWidget *widget;
  glong read_count, write_count, i, utf8_len, expand_index = 0;

#ifdef USE_PANGO_LAYOUT
  PangoLayout *layout;
  struct _zvtprivate *zp = 0;
  zp = _ZVT_PRIVATE(term);
#endif

  widget = GTK_WIDGET (term);

  for (i = col; i < col + len; i++) {
    gunichar uc = line->data[i] & VTATTR_DATAMASK;
    if (uc == 0 && !((line->data[i] & VTATTR_MASK) & VTATTR_UTF16_HALF)) {
      utf8_len = 1;
      utf8_ptr[expand_index] = ' ';
    } else if (uc == '\t') {
      utf8_len = 1;
      utf8_ptr[expand_index] = ' ';
    } else {
      if (line->data[i] & VTATTR_UTF16_HALF) {
	uc = (((uc << 16) & 0xffff0000) | (line->data[++i] & VTATTR_DATAMASK));
      }
      utf8_len = g_unichar_to_utf8 (uc, &(utf8_ptr[expand_index]));
    }
    expand_index += utf8_len;
  }

#ifdef USE_PANGO_LAYOUT
  zp->pango_context = gdk_pango_context_get();
  pango_context_set_language (zp->pango_context,
			      gtk_get_default_language ());
  gdk_pango_context_set_colormap(zp->pango_context, term->color_map);
  pango_context_set_font_description(zp->pango_context, zp->pango_font);
  layout = pango_layout_new(zp->pango_context);
  pango_layout_set_text(layout, utf8_ptr, expand_index);

  if (dofill) {
    GdkColor *fcolor = &(term->colors[fore]);
    GdkColor *bcolor = &(term->colors[back]);
    gdk_draw_layout_with_colors(widget->window, fgc, x + offx, y + offy, layout, fcolor, bcolor);
  } else {
    gdk_draw_layout(widget->window, fgc, x + offx, y + offy, layout);
  }

  g_object_unref (layout);
#else
  if (utf8_ptr) {
    GError *error = NULL;
    gsize locale_format_len = 0;
    gchar *locale_format;
    G_CONST_RETURN char *locale_encoding;

    if (g_get_charset(&locale_encoding)) {
      locale_format = g_strdup(utf8_ptr);
      locale_format_len = expand_index;
    } else
      locale_format = zvt_term_locale_from_utf8 (utf8_ptr, expand_index,
						 NULL, &locale_format_len, &error);
    if (error)
      {
	g_warning ("Error converting from utf8: %s\n", error->message);
	if (error)
	  g_error_free (error);
      }
    else
      {
	gint real_offx, real_offy;
	GdkDrawable *real_drawable;
	if (term->xfontset) {
	  /* call XmbDrawString here with XFontSet */
	  
	  gdk_window_get_internal_paint_info(widget->window,
					     &real_drawable,
					     &real_offx,&real_offy);
	  offx-=real_offx;
	  offy-=real_offy;

#define GET_VALUE_MASK	(GCFunction | GCForeground | GCBackground | GCFillStyle)
	  {
	    int startx = offx + x;
	    gchar locale_char;
	    gchar *bufp = locale_format;
	    gchar *endp = locale_format + locale_format_len;
	    int mb_len;

	    Display *display = GDK_WINDOW_XDISPLAY(widget->window);
	    Drawable drawable = GDK_WINDOW_XWINDOW(real_drawable);
	    XGCValues  values;
	    GC gc = XCreateGC(display, drawable, 0, NULL);

	    XGetGCValues(display, GDK_GC_XGC(fgc),
			 GET_VALUE_MASK, &values);

	    XSetFunction(display, gc, GXcopy);
	    XSetFillStyle(display, gc, FillSolid);
	    XSetForeground(display, gc, values.background);

	    while (bufp < endp && (mb_len = mblen(bufp, MB_CUR_MAX)) > 0) {
	      /* draw one multi-byte character at a time, with fill
		 if we can */
	      int text_width, spacing;
	      int column_width_of_char;
	      XRectangle extent;
	      gboolean is_wide;

	      /* fill spacing to the next character if any */
	      text_width = XmbTextExtents(term->xfontset, bufp, mb_len,
					  0, &extent);
	      is_wide = localechar_iswide_internal (bufp, mb_len);
	      spacing = term->charwidth * (is_wide ? 2 : 1) - extent.width;

	      if (dofill) {
		XmbDrawImageString(GDK_WINDOW_XDISPLAY(widget->window),
				   GDK_WINDOW_XWINDOW(real_drawable),
				   term->xfontset, GDK_GC_XGC(fgc),
				   startx, offy + y,
				   bufp, mb_len);
	      } else {
		XmbDrawString(GDK_WINDOW_XDISPLAY(widget->window),
			      GDK_WINDOW_XWINDOW(real_drawable),
			      term->xfontset, GDK_GC_XGC(fgc),
			      startx, offy + y,
			      bufp, mb_len);
	      }
	      if (dofill && spacing > 0)
		XFillRectangle(GDK_WINDOW_XDISPLAY(widget->window),
			       drawable,
			       gc, startx + extent.width,
			       offy + y + extent.y,
			       spacing, extent.height);
	      if (overstrike)
		XmbDrawString(GDK_WINDOW_XDISPLAY(widget->window),
			      GDK_WINDOW_XWINDOW(real_drawable),
			      term->xfontset, GDK_GC_XGC(fgc),
			      startx + 1, offy + y,
			      bufp, mb_len);
	      startx += term->charwidth * (is_wide ? 2 : 1);
	      bufp += mb_len;
	    }
	    XFreeGC(display, gc);
	  }
	  offx+=real_offx;
	  offy+=real_offy;
	}
      }
    g_free (locale_format);
  }
#endif /* !USE_PANGO_LAYOUT */
}

static void
zvt_term_commit_handler (GtkIMContext  *context,
			 const gchar   *str,
			 ZvtTerm       *term)
{
  gsize length = 0;
  gchar *locstr = zvt_term_locale_from_utf8 (str, -1, NULL, &length, NULL);

  if (length > 0) {
    vt_writechild (&term->vx->vt, locstr, length);
    if (term->scroll_on_keystroke) zvt_term_scroll (term, 0);
  }
  g_free (locstr);
}

static GdkWindow *
zvt_make_preedit_window (GtkWidget *widget) {
    GdkWindowAttr window_attr;
    gint attr_mask;
    window_attr.wclass = GDK_INPUT_OUTPUT;
    window_attr.window_type = GDK_WINDOW_CHILD;
    window_attr.event_mask = GDK_EXPOSURE_MASK;
    window_attr.visual = gtk_widget_get_visual(widget);
    window_attr.colormap = gtk_widget_get_colormap(widget);
    attr_mask = GDK_WA_X | GDK_WA_Y | GDK_WA_VISUAL | GDK_WA_COLORMAP;
    return gdk_window_new(widget->window, &window_attr, attr_mask);
}

static void
zvt_resize_preedit_window (ZvtTerm *term, GdkWindow *window,
			   gint x, gint y, gint width) {
  gint space;

  if (width > term->vx->vt.width)
    width = term->vx->vt.width;

  space = term->vx->vt.width - term->vx->vt.cursorx;
  if (space < width)
    x -= (width - space) * term->charwidth;

  gdk_window_move_resize(window, x, y,
			 width * term->charwidth, term->charheight);
}

#define PREEDIT_UNDERLINE_WIDTH 1

static void
zvt_term_preedit_changed_handler (GtkIMContext *context,
				  ZvtTerm *term)
{
  gchar *preedit_string = NULL;
  gint cursor_pos = 0;
  struct _vtx *vx;
  PangoAttrList *feedback_list = NULL;
  PangoAttrIterator *iter;
  PangoLayout *layout;
  struct _zvtprivate *zp = 0;
  gint x, y;
  gint preedit_len, preedit_col_len, preedit_advanced;
  gint attr_start, attr_end, attr_length, attr_x;
  struct vt_line *line;

  zp = _ZVT_PRIVATE(term);

  gtk_im_context_get_preedit_string (context,
				     &preedit_string,
				     &feedback_list,
				     &cursor_pos);
  
  preedit_len = strlen(preedit_string);
  if (preedit_string == NULL || preedit_len <= 0) {
    if (zp->preedit_window != NULL && gdk_window_is_visible(zp->preedit_window)) {
      gdk_window_hide(zp->preedit_window);
    }
    return;
  }

  if (zp->preedit_window == NULL) { 
    /* create over the spot preedit window */
    zp->preedit_window = zvt_make_preedit_window(GTK_WIDGET(term));
  }

  preedit_col_len = zvt_column_preedit_string (preedit_string, preedit_len);
  cursor_pos = CLAMP (cursor_pos, 0, g_utf8_strlen (preedit_string, -1));
  
  /* adjust size of preedit window */
  vx = term->vx;
  x = vx->vt.cursorx * term->charwidth + GTK_WIDGET(term)->style->xthickness + term->padding;
  y = vx->vt.cursory * term->charheight + GTK_WIDGET(term)->style->ythickness;

  zvt_resize_preedit_window(term, zp->preedit_window, x, y, preedit_col_len);
  gdk_window_show(zp->preedit_window);

  /* draw preedit string with attribute */ 
  zp->pango_context = gdk_pango_context_get();
  pango_context_set_language (zp->pango_context,
			      gtk_get_default_language ());
  gdk_pango_context_set_colormap(zp->pango_context, term->color_map);

  pango_context_set_font_description(zp->pango_context, zp->pango_font);

  layout = pango_layout_new(zp->pango_context);
  iter = pango_attr_list_get_iterator(feedback_list);

  attr_x = 0;
  do {
    GSList *attrs = NULL;
    GSList *attr_list;
    gboolean is_reverse, is_underline;

    pango_attr_iterator_range(iter, &attr_start, &attr_end);
    attr_end = CLAMP(attr_end, attr_start, preedit_len);
    attr_length = attr_end - attr_start;
    if (attr_length <= 0)
      continue;

    pango_attr_iterator_get_font (iter, zp->pango_font, NULL, &attrs);
    is_underline = FALSE;
    is_reverse = FALSE;

    attr_list = attrs;
    while (attr_list) {
      PangoAttribute *attr = attr_list->data;
      switch(attr->klass->type) {
      case PANGO_ATTR_FOREGROUND:
	/* FIX Later: should keep color */
	is_reverse = TRUE;
	break;
      case PANGO_ATTR_BACKGROUND:
	/* FIX Later: should keep color */
	is_reverse = TRUE;
	break;
      case PANGO_ATTR_UNDERLINE:
	is_underline = TRUE;
	break;
      case PANGO_ATTR_STRIKETHROUGH:
	break;
      default:
	g_warning("Unknown preedit attribute type: %d\n", attr->klass->type);
	break;
      }
      pango_attribute_destroy(attr);
      attr_list = attr_list->next;
    }

    g_slist_free (attrs);

    pango_layout_set_text(layout, preedit_string + attr_start, attr_length);
    if (is_reverse) {
      gdk_draw_layout_with_colors(zp->preedit_window, term->fore_gc, attr_x, 0, layout,
				  &(term->colors[17]), &(term->colors[16]));
    } else {
      gdk_draw_layout_with_colors(zp->preedit_window, term->fore_gc, attr_x, 0, layout,
				  &(term->colors[16]), &(term->colors[17]));
    }

    preedit_advanced = zvt_column_from_utf8 (preedit_string + attr_start, attr_length) * term->charwidth;
    if (is_underline) {
      gdk_gc_set_foreground(term->fore_gc, &(term->colors[16]));
      gdk_draw_rectangle(zp->preedit_window, term->fore_gc, 1,
			 attr_x, term->charheight - PREEDIT_UNDERLINE_WIDTH,
			 preedit_advanced, PREEDIT_UNDERLINE_WIDTH);
    }
    attr_x +=  preedit_advanced;
  } while(pango_attr_iterator_next(iter));

  pango_attr_iterator_destroy (iter);
  pango_attr_list_unref (feedback_list);
  g_object_unref (layout);
  g_free (preedit_string);
}

/* Public functions for Input Methods */
void
zvt_term_im_new (ZvtTerm *term)
{
  struct _zvtprivate *zp;
  zp = _ZVT_PRIVATE(term);

  if (!term->ic) {
    term->ic = gtk_im_multicontext_new ();
    g_signal_connect (term->ic, "commit",
		      G_CALLBACK (zvt_term_commit_handler), term);

    gtk_im_context_set_client_window (term->ic, GTK_WIDGET(term)->window);
    
    g_signal_connect (G_OBJECT(term->ic), "preedit-changed",
		      GTK_SIGNAL_FUNC(zvt_term_preedit_changed_handler), term);
    zp->preedit_window = 0;
  }
  return;
}

void
zvt_term_im_cursor_location (ZvtTerm *term)
{
  /* 
     Some input methods may use this position to show their
     status-bar and lookup-choice windows.
     Adding +1 offset to both cursorx and cursory to keep
     these windows from hiding the text at the cursor location.
  */
  if (term->ic) {
    GdkRectangle area;
    area.x = (term->vx->vt.cursorx + 1) * term->charwidth;
    area.y = (term->vx->vt.cursory + 1) * term->charheight;
    area.width = area.height = 0;
    gtk_im_context_set_cursor_location (term->ic, &area);
  }
}

void
zvt_term_preedit_update (ZvtTerm *term)
{
  struct _zvtprivate *zp;
  zp = _ZVT_PRIVATE(term);

  if (zp->preedit_window != NULL &&
      gdk_window_is_visible (zp->preedit_window))
    zvt_term_preedit_changed_handler (term->ic, term);
}

void
zvt_term_im_reset (ZvtTerm *term)
{
#ifdef NEED_IM_RESET
  if (term->ic && term->need_im_reset) {
    term->need_im_reset = FALSE;
    gtk_im_context_reset (term->ic);
  }
#endif
}

void
zvt_term_im_free (ZvtTerm *term)
{
  g_warning("not implemented yet");
}

/* FontSet */

/*
  xlfd_from_pango_font_description copied from vte, which was
  written by nalin@redhat.com, and added some codes.
*/
static char *
xlfd_from_pango_font_description(GtkWidget *widget,
				 const PangoFontDescription *fontdesc)
{
	char *spec;
	PangoContext *context;
	PangoFont *font;
	PangoXSubfont *subfont_ids;
	PangoFontMap *fontmap;
	int *subfont_charsets, i, count = 0;
	char *xlfd = NULL, *tmp, *subfont;
	char *encodings[] = {
		"ascii-0",
		"big5-0",
		"dos-437",
		"dos-737",
		"gb18030.2000-0",
		"gb18030.2000-1",
		"gb2312.1980-0",
		"iso8859-1",
		"iso8859-2",
		"iso8859-3",
		"iso8859-4",
		"iso8859-5",
		"iso8859-7",
		"iso8859-8",
		"iso8859-9",
		"iso8859-10",
		"iso8859-15",
		"iso10646-0",
		"iso10646-1",
		"jisx0201.1976-0",
		"jisx0208.1983-0",
		"jisx0208.1990-0",
		"jisx0208.1997-0",
		"jisx0212.1990-0",
		"jisx0213.2000-1",
		"jisx0213.2000-2",
		"koi8-r",
		"koi8-u",
		"koi8-ub",
		"ksc5601.1987-0",
		"ksc5601.1992-3",
		"tis620-0",
		"iso8859-13",
		"microsoft-cp1251"
		"misc-fontspecific",
	};
#if XlibSpecificationRelease >= 6
	XOM xom;
#endif
	g_return_val_if_fail(fontdesc != NULL, NULL);
	g_return_val_if_fail(GTK_IS_WIDGET(widget), NULL);

	context = gtk_widget_get_pango_context(GTK_WIDGET(widget));

	pango_context_set_language (context,
				    gtk_get_default_language ());
	fontmap = pango_x_font_map_for_display(GDK_DISPLAY());
	if (fontmap == NULL) {
		return g_strdup("fixed");
	}

	font = pango_font_map_load_font(fontmap, context, fontdesc);
	if (font == NULL) {
		return g_strdup("fixed");
	}

#if XlibSpecificationRelease >= 6
	xom = XOpenOM (GDK_DISPLAY(), NULL, NULL, NULL);
	if (xom) {
	  XOMCharSetList cslist;
	  int n_encodings = 0;

	  cslist.charset_count = 0;
	  XGetOMValues (xom,
			XNRequiredCharSet, &cslist,
			NULL);
	  n_encodings = cslist.charset_count;
	  if (n_encodings) {
	    char **xom_encodings = g_malloc (sizeof(char*) * n_encodings);

	    for (i = 0; i < n_encodings; i++)
	      xom_encodings[i] = g_ascii_strdown (cslist.charset_list[i], -1);
	    count = pango_x_list_subfonts(font, xom_encodings, n_encodings,
					  &subfont_ids, &subfont_charsets);

	    for(i = 0; i < n_encodings; i++)
	      g_free (xom_encodings[i]);
	    g_free (xom_encodings);
	  }
	  XCloseOM (xom);
	}
#endif
	if (count == 0)
	  count = pango_x_list_subfonts(font, encodings, G_N_ELEMENTS(encodings),
					&subfont_ids, &subfont_charsets);

	for (i = 0; i < count; i++) {
		subfont = pango_x_font_subfont_xlfd(font, subfont_ids[i]);
		if (xlfd) {
			tmp = g_strconcat(xlfd, ",", subfont, NULL);
			g_free(xlfd);
			g_free(subfont);
			xlfd = tmp;
		} else {
			xlfd = subfont;
		}
	}

	spec = pango_font_description_to_string(fontdesc);

	if (subfont_ids != NULL) {
		g_free(subfont_ids);
	}
	if (subfont_charsets != NULL) {
		g_free(subfont_charsets);
	}
	g_free(spec);

	return xlfd;
}

static const gchar *ASCII_CHARACTERS = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ !\"#$%&'()*+,-./:;<=>?@\\]^_`{}~";

static const gchar *DEFAULT_XLFD_NAME = "-*-*-*-*-*-*-%i-*-*-*-*-*-*-*";

static XFontSet
zvt_term_get_xfontset_internal (ZvtTerm *term,
				const char *fontset_name,
				int *new_charwidth, 
				int *new_charheight,
				int *new_fontascent)
{
  gint  missing_charset_count;
  gchar **missing_charset_list;
  gchar *def_string;
  XFontSet new_fontset = 0;

  new_fontset = XCreateFontSet (GDK_DISPLAY (), fontset_name,
				&missing_charset_list,
				&missing_charset_count,
				&def_string);
  if (missing_charset_list != NULL) {
    XFreeStringList(missing_charset_list);
    missing_charset_list = NULL;
  }

  if (new_fontset) {
    XFontStruct **xfsList = 0;
    char **fontnameList;
    gint i;
    gint max_percharwidth = 0;
    gint max_percharheight = 0;
    XRectangle extent;
    gchar *bufp = (gchar*)ASCII_CHARACTERS;
    gint char_len = strlen(bufp);

    /* should be a little faster than using XmbTextPerCharExtents... */
    for (i = 0; i < char_len; i++) {
      if (XmbTextExtents(new_fontset, bufp++, 1,
			 0, &extent) > 0) {
	max_percharwidth = (extent.width > max_percharwidth ?
			    extent.width : max_percharwidth);
	max_percharheight = (extent.height > max_percharheight ?
			     extent.height : max_percharheight);
      }
    }
    *new_charwidth = max_percharwidth;
    *new_charheight = max_percharheight;

    XFontsOfFontSet (new_fontset, &xfsList, &fontnameList);
    if (xfsList && xfsList[0])
      *new_fontascent = xfsList[0]->ascent;

    if (*new_charwidth == 0 || *new_charheight == 0 ||
	*new_fontascent == 0) {
      if (new_fontset)
	XFreeFontSet (GDK_DISPLAY(), new_fontset);
      new_fontset = 0;
    }
  }
  return new_fontset;
}

static void
zvt_term_get_xfontset (ZvtTerm *term,
		       const PangoFontDescription *font_desc)
{
  struct _zvtprivate *zp;
  GtkWidget *widget;
  gchar *fontset_name = 0;
  XFontSet new_fontset = 0;
  int new_charwidth = 0;
  int new_charheight = 0;
  int new_fontascent = 0;

  zp = _ZVT_PRIVATE (term);
  widget = GTK_WIDGET (term);

  fontset_name = xlfd_from_pango_font_description(GTK_WIDGET(widget),
						  font_desc);
  if (fontset_name) {
    new_fontset = zvt_term_get_xfontset_internal (term, fontset_name, 
						  &new_charwidth,
						  &new_charheight,
						  &new_fontascent);
    g_free(fontset_name);
  }

  if (new_fontset == 0 || new_charwidth == 0 || new_charheight == 0) {
    guint font_size = pango_font_description_get_size(font_desc)/PANGO_SCALE;
    g_warning ("Cannot get required fonts from \"%s %i\"",
	       pango_font_description_get_family (font_desc),
	       font_size);

    fontset_name = g_strdup_printf (DEFAULT_XLFD_NAME, font_size);
    g_warning ("Use \"%s\" XLFD fontname - some characters may not appear correctly\n", fontset_name);

    new_fontset = zvt_term_get_xfontset_internal (term, fontset_name,
						  &new_charwidth,
						  &new_charheight,
						  &new_fontascent);
    g_free (fontset_name);

    if (new_fontset == 0 || new_charwidth == 0 || new_charheight == 0) {
      fontset_name = g_strdup_printf (DEFAULT_XLFD_NAME, 0);
      g_warning ("Use \"%s\" XLFD fontname - some characters may not appear correctly\n", fontset_name);
      new_fontset = zvt_term_get_xfontset_internal (term, fontset_name,
						    &new_charwidth,
						    &new_charheight,
						    &new_fontascent);
      g_free (fontset_name);
    }
  }
  g_return_if_fail (new_fontset != NULL);
  g_return_if_fail (new_charwidth != 0);
  g_return_if_fail (new_charheight != 0);
  g_return_if_fail (new_fontascent!= 0);

  if (term->xfontset)
    XFreeFontSet (GDK_DISPLAY(), term->xfontset);
  term->xfontset = new_fontset;
  term->charwidth = new_charwidth;
  term->charheight = new_charheight;
  term->font_ascent = new_fontascent;

  return;
}

/**
 * zvt_term_set_pango_font:
 * @term: A &ZvtTerm widget.
 * @font_desc: Pango Font used for regular text.
 *
 * Load pango font into the terminal.
 * 
 * These fonts should be the same size, otherwise it could get messy ...
 */
void
zvt_term_set_pango_font (ZvtTerm *term,
			 const PangoFontDescription *font_desc)
{
  PangoFontDescription *font_bold_desc;
  PangoFontset *fontset;
  PangoFontMetrics *metrics;
  struct _zvtprivate *zp;

  g_return_if_fail (term != NULL);
  g_return_if_fail (ZVT_IS_TERM (term));
  g_return_if_fail (font_desc != NULL);

  zp = _ZVT_PRIVATE(term);

  fontset = pango_context_load_fontset(gdk_pango_context_get(),
				       font_desc, gtk_get_default_language());
  metrics = pango_fontset_get_metrics(fontset);

  gtk_widget_queue_resize (GTK_WIDGET (term));

#ifdef USE_PANGO_LAYOUT
  term->font_ascent = pango_font_metrics_get_ascent(metrics)/PANGO_SCALE;
  term->charheight = (pango_font_metrics_get_ascent(metrics) +
		      pango_font_metrics_get_decent(metrics))/PANGO_SCALE;
  term->charwidth = pango_font_metrics_get_approximate_digit_width(metrics)/PANGO_SCALE;
#else
  zvt_term_get_xfontset (term, font_desc);
#endif

  if (term->xfontset == NULL ||
      term->charwidth == 0 || term->charheight == 0) {
    g_warning ("Failed to load any required font, so crash will occur.." "check X11 font pathes and etc/pangox.alias file\n");
  }

  if (zp->pango_font)
    pango_font_description_free(zp->pango_font);
  if (zp->pango_font_bold)
    pango_font_description_free(zp->pango_font_bold);

  zp->pango_font = pango_font_description_copy(font_desc);
  font_bold_desc = pango_font_description_copy(font_desc);
  pango_font_description_set_weight(font_bold_desc, PANGO_WEIGHT_BOLD);

  zp->pango_font_bold = font_bold_desc;

  return;

}

/**
 * zvt_term_get_utf8_from_line:
 * @line: pointer to struct vt_line
 * @ucs4: pointer to ucs4 char array
 *
 * retruns allocated utf8 string with null terminate from vt_line data
 */
gchar*
zvt_term_get_utf8_from_line (struct vt_line *line)
{
  gint i, utf8_len, expand_index = 0;

  gchar *utf8_buf = g_malloc (line->width * 4);
  for (i = 0; i < line->width; i++) {
    gunichar uc = line->data[i] & VTATTR_DATAMASK;
    if (uc == 0 && !(line->data[i] & VTATTR_UTF16_HALF)) {
      utf8_len = 1;
      utf8_buf[expand_index] = ' ';
    } else if (uc == '\t') {
      utf8_len = 1;
      utf8_buf[expand_index] = ' ';
    } else {
      if (line->data[i] & VTATTR_UTF16_HALF) {
	uc = (((uc << 16) & 0xffff0000) | (line->data[++i] & VTATTR_DATAMASK));
      }
      utf8_len = g_unichar_to_utf8 (uc, &(utf8_buf[expand_index]));
    }
    expand_index += utf8_len;
  }
  utf8_buf[expand_index] = '\0';
  return utf8_buf;
}

/**
 * zvt_term_get_char_index_from_column:
 * @column_index: column index
 *
 * returns char index corresponds to column index
 * character index and column is not same as one wide character occupy two columns
 */
gint
zvt_term_get_char_index_from_column (gchar *utf8_ptr, gint column_index)
{
  gint i = 0, char_index = 0;

  for (;;) {
    gunichar uc = g_utf8_get_char (utf8_ptr);
    utf8_ptr = g_utf8_next_char (utf8_ptr);
    if (zvt_localechar_iswide (uc)) {
      i += 2;
    } else {
      i++;
    }
    if (i > column_index)
      break;

    char_index++;
  }

  return char_index;
}

/**
 * zvt_term_get_column_from_char_index:
 * @char_index: character index
 *
 * returns column index corresponds to character index of line
 * character index and column is not same as one wide character occupy two columns
 */
gint
zvt_term_get_column_from_char_index (gchar *utf8_ptr, gint char_index)
{
  gint i, column_count = 0;

  for (i = 0; i < char_index; i++) {
    gunichar uc = g_utf8_get_char (utf8_ptr);
    utf8_ptr = g_utf8_next_char (utf8_ptr);
    if (zvt_localechar_iswide (uc)) {
      column_count++;
    }
    column_count++;
  }

  return column_count;
}
#endif /* HAS_I18N */
