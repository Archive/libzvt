#undef ENABLE_NLS
#undef HAVE_CATGETS
#undef HAVE_GETTEXT
#undef HAVE_LC_MESSAGES
#undef HAVE_STPCPY

#undef HAVE_STROPTS_H

/* We define macro, which expands to utmp or utmpx when available */
#undef UTMP

#undef HAVE_UT_UT_PID
#undef HAVE_UT_UT_ID
#undef HAVE_UT_UT_USER
#undef HAVE_UT_UT_NAME
#undef HAVE_UT_UT_TYPE
#undef HAVE_UT_UT_HOST
#undef HAVE_UT_UT_TV
#undef HAVE_UT_UT_TIME
#undef HAVE_UT_UT_SYSLEN
#undef HAVE_UT_UT_EXIT_E_TERMINATION

#undef HAVE_SETEUID
#undef HAVE_SETREUID

#undef HAVE_OPENPTY
#undef HAVE_LOGIN_TTY
#undef HAVE_SENDMSG

/* Defined, if there is struct lastlog */
#undef HAVE_LASTLOG

/* define if needed to include sa_len member in struct sockaddr */
#undef _SOCKADDR_LEN

#undef HAVE_LIBUTIL
